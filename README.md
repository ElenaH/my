### Public projects

[Vali IT! group project] (https://gitlab.com/friedballerina/GroupProject)

[Test task 1](https://gitlab.com/ElenaH/GamingServer)

[Test task 1 with tests ... in progress] (https://gitlab.com/ElenaH/ServerTDD)

[Test task 2] (https://gitlab.com/ElenaH/knDecathlon)

[Test task 3] (https://gitlab.com/ElenaH/HiLoGame)